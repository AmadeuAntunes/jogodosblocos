﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bola : MonoBehaviour
{
    public bool JogoIniciou;
    public float DireccaoX;
    public float DirecaoY;
    public Rigidbody2D Rigidbody2D;
    public AudioSource SomDaBola;
    public float DirecaoAleatoriaX;
    public float DirecaoAleatoriaY;
    // Start is called before the first frame update
    void Start()
    {
        JogoIniciou = false;
    }

    // Update is called once per frame
    void Update()
    {
       
        if(Input.GetKeyDown(KeyCode.Space))
        {
            LancarBola();
        }
        
    }

    private void LancarBola()
    {
        if(!JogoIniciou)
        {
            Rigidbody2D.velocity = new Vector2(DireccaoX, DirecaoY);
            JogoIniciou = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        SomDaBola.Play();
        Rigidbody2D.velocity += new Vector2(DirecaoAleatoriaX, DirecaoAleatoriaY);
    }
}
