﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public int QuantidadeDeBlocos;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SairJogo();
        }
    }

    public void AumentarQuantidadeDeBlocos()
    {
        QuantidadeDeBlocos += 1;
    }

    public void DiminuirQuantidadeDeBlocos()
    {
        QuantidadeDeBlocos -= 1;
        if(QuantidadeDeBlocos == 0)
        {
            if(SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            else
            {
                SceneManager.LoadScene(0);
            }
          
        }
    }

    private void SairJogo()
    {
        Application.Quit();
    }
}
