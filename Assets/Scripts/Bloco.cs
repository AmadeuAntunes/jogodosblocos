﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloco : MonoBehaviour
{
    public GameObject SomDoBloco;

    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<GameManager>().AumentarQuantidadeDeBlocos();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
          if(other.gameObject.tag == "Bola")
            {
                FindObjectOfType<GameManager>().DiminuirQuantidadeDeBlocos();
                var som = Instantiate(SomDoBloco, transform.position, transform.rotation);
                Destroy(som.gameObject,3);
                Destroy(this.gameObject);
            }
    }
}
